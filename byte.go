package util

func CopyBytes(src []byte) (dst []byte) {
	if len(src) > 0 {
		dst = make([]byte, len(src))
		copy(dst, src)
	}
	return
}
