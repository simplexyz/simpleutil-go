package util

import "fmt"

func Assert(b bool, text string) {
	if b {
		panic(text)
	}
}

func Assertf(b bool, format string, args ...any) {
	if b {
		panic(fmt.Sprintf(format, args...))
	}
}
